package com.davidmendano.oompaloompa

import android.app.Application
import com.davidmendano.oompaloompa.di.AppModule
import com.davidmendano.oompaloompa.di.DaggerDiComponent
import com.davidmendano.oompaloompa.di.DiComponent

class OLApplication: Application() {

    companion object {
        lateinit var instance: OLApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun getDagger(): DiComponent {
        return DaggerDiComponent.builder()
            .appModule(AppModule(instance))
            .build()
    }
}