package com.davidmendano.oompaloompa.db

import androidx.room.TypeConverter
import com.davidmendano.oompaloompa.network.model.FavoriteModel
import com.google.gson.Gson

class OLConverters {

    @TypeConverter
    fun fromStringFavorite(json: String?): FavoriteModel {
        if (json == null) return FavoriteModel()

        return Gson().fromJson(json, FavoriteModel::class.java)
    }

    @TypeConverter
    fun fromFavorite(value: FavoriteModel?): String {
        return value?.toString() ?: ""
    }

}