package com.davidmendano.oompaloompa.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel

@Dao
interface OLDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOompaLoompas(olData: MutableList<OompaLoompaModel?>)

    @Query("SELECT * FROM oompa_loompa_data WHERE id=:id ")
    suspend fun findOompaLoompaById(id: Int): OompaLoompaModel?
}