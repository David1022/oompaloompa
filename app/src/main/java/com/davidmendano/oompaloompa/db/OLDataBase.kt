package com.davidmendano.oompaloompa.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.davidmendano.oompaloompa.network.model.FavoriteModel
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel

@Database(
    entities = [OompaLoompaModel::class, FavoriteModel::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(OLConverters::class)
abstract class OLDataBase : RoomDatabase() {
    abstract fun olDao(): OLDao
}