package com.davidmendano.oompaloompa.di

import com.davidmendano.oompaloompa.OLApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: OLApplication) {

    @Provides
    @Singleton
    fun providesApplication(): OLApplication = application

}
