package com.davidmendano.oompaloompa.di

import androidx.room.Room
import com.davidmendano.oompaloompa.OLApplication
import com.davidmendano.oompaloompa.db.OLDao
import com.davidmendano.oompaloompa.db.OLDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {
    @Singleton
    @Provides
    fun provideOLDataBase(application: OLApplication): OLDataBase {
        return Room.databaseBuilder(
            application.applicationContext,
            OLDataBase::class.java,
            "oompaLoompaDB"
        )
            .build()
    }

    @Singleton
    @Provides
    fun provideOLDao(olDataBase: OLDataBase): OLDao {
        return olDataBase.olDao()
    }
}