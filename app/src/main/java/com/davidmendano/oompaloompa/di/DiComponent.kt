package com.davidmendano.oompaloompa.di

import com.davidmendano.oompaloompa.ui.detail.DetailFragment
import com.davidmendano.oompaloompa.ui.homelist.HomeListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RepositoryModule::class,
        AppModule::class,
        DataBaseModule::class
    ]
)
interface DiComponent {
    fun inject(homeListFragment: HomeListFragment)
    fun inject(detailFragment: DetailFragment)
}