package com.davidmendano.oompaloompa.di

import com.davidmendano.oompaloompa.db.OLDao
import com.davidmendano.oompaloompa.network.OLApiService
import com.davidmendano.oompaloompa.network.OLRepository
import com.davidmendano.oompaloompa.network.OLRepositoryImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RepositoryModule {

    val BASE_URL = "https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/"

    @Provides
    fun providesRepositoryService(retrofit: Retrofit): OLApiService =
        retrofit.create(OLApiService::class.java)

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    fun providesOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()

    @Provides
    fun providesLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    fun providesOLRepository(service: OLApiService, olDao: OLDao): OLRepository =
        OLRepositoryImpl(service, olDao)
}