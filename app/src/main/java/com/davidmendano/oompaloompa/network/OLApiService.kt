package com.davidmendano.oompaloompa.network

import com.davidmendano.oompaloompa.network.model.OLListModel
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface OLApiService {

    @GET("oompa-loompas")
    suspend fun getOLList(
        @Query("page") page: Int = 1
    ): OLListModel

    @GET("oompa-loompas/{uid}")
    suspend fun getOLDetail(
        @Path("uid") id: Int
    ): OompaLoompaModel

}