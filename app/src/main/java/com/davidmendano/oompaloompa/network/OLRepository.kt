package com.davidmendano.oompaloompa.network

import com.davidmendano.oompaloompa.network.base.ApiWrapper
import com.davidmendano.oompaloompa.network.model.OLListModel
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel

interface OLRepository {

    suspend fun getOLList(page: Int): ApiWrapper<OLListModel>

    suspend fun getOLDetail(id: Int): ApiWrapper<OompaLoompaModel>
}