package com.davidmendano.oompaloompa.network

import com.davidmendano.oompaloompa.db.OLDao
import com.davidmendano.oompaloompa.network.base.ApiWrapper
import com.davidmendano.oompaloompa.network.base.BaseRepository
import com.davidmendano.oompaloompa.network.model.OLListModel
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel
import javax.inject.Inject

class OLRepositoryImpl @Inject constructor(
    private val service: OLApiService,
    private val olDao: OLDao
) : BaseRepository(),
    OLRepository {

    override suspend fun getOLList(page: Int): ApiWrapper<OLListModel> {
        val response = apiCall {
            service.getOLList(page)
        }
        if (response is ApiWrapper.Success) {
            response.value.results?.let { list ->
                olDao.insertOompaLoompas(list)
            }
        }

        return response
    }

    override suspend fun getOLDetail(id: Int): ApiWrapper<OompaLoompaModel> {
        val datafromDB = getDetailFromDB(id)
        if (datafromDB == null) {
            return getDataFromApi(id)
        } else {
            return ApiWrapper.Success(datafromDB)
        }
    }

    private suspend fun getDetailFromDB(id: Int): OompaLoompaModel? = olDao.findOompaLoompaById(id)

    private suspend fun getDataFromApi(id: Int): ApiWrapper<OompaLoompaModel> =
        apiCall {
            service.getOLDetail(id)
        }
}