package com.davidmendano.oompaloompa.network.base

sealed class ApiWrapper<out T> {

    data class Success<out T>(val value: T) : ApiWrapper<T>()

    object ServerError : ApiWrapper<Nothing>()

    object NetworkError : ApiWrapper<Nothing>()
}