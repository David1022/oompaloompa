package com.davidmendano.oompaloompa.network.base

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

open class BaseRepository {
    suspend fun <T> apiCall(
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
        apiCall: suspend () -> T
    ): ApiWrapper<T> {
        return withContext(dispatcher) {
            try {
                ApiWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> ApiWrapper.NetworkError
                    else -> ApiWrapper.ServerError
                }
            }
        }
    }
}