package com.davidmendano.oompaloompa.network.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class FavoriteModel(

    @field:SerializedName("song")
    var song: String = "",

    @PrimaryKey
    @field:SerializedName("random_string")
    var randomString: String = "",

    @field:SerializedName("color")
    var color: String = "",

    @field:SerializedName("food")
    var food: String = ""
) : Parcelable {

    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this)
    }

    fun fromJson(json: String): FavoriteModel {
        return Gson().fromJson(json, FavoriteModel::class.java)
    }
}
