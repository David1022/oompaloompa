package com.davidmendano.oompaloompa.network.model

import com.google.gson.annotations.SerializedName

data class OLListModel(

	@field:SerializedName("current")
	val current: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("results")
	val results: MutableList<OompaLoompaModel?>? = null
)
