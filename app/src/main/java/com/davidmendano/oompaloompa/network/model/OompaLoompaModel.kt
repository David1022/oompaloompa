package com.davidmendano.oompaloompa.network.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.davidmendano.oompaloompa.utils.Gender
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "oompa_loompa_data")
@Parcelize
data class OompaLoompaModel(

    @field:SerializedName("profession")
    var profession: String = "",

    @field:SerializedName("image")
    var image: String = "",

    @field:SerializedName("country")
    var country: String = "",

    @field:SerializedName("gender")
    var gender: String = "",

    @field:SerializedName("last_name")
    var lastName: String = "",

    @PrimaryKey
    @field:SerializedName("id")
    var id: Int = -1,

    @field:SerializedName("first_name")
    var firstName: String = "",

    @field:SerializedName("favorite")
    var favorite: FavoriteModel? = null,

    @field:SerializedName("email")
    var email: String = "",

    @field:SerializedName("age")
    var age: Int = 0,

    @field:SerializedName("height")
    var height: Int = 0
) : Parcelable {
    val formattedName
        get() = "$firstName $lastName"

    val formattedGender
        get() =
            if (gender == Gender.MALE.apiValue) Gender.MALE.formattedValue
            else Gender.FEMALE.formattedValue

    val formattedAge
        get() = age.toString()

    val formattedHeight
        get() = height.toString()
}
