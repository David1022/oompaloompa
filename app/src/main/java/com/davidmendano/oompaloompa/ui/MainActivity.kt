package com.davidmendano.oompaloompa.ui

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    fun showLoader(showLoader: Boolean?) {
        binding.loader.visibility =
            if (showLoader == true)
                View.VISIBLE
            else
                View.GONE
    }

    fun showNetworkError(@StringRes errorMessage: Int?, listener: () -> Unit) {
        AlertDialog.Builder(this)
            .setTitle(R.string.network_error)
            .setMessage(errorMessage ?: R.string.generic_error)
            .setNeutralButton(R.string.retry) { _, _ ->
                listener.invoke()
            }
            .setCancelable(false)
            .show()
    }
}