package com.davidmendano.oompaloompa.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.davidmendano.oompaloompa.databinding.ItemLoaderBinding

class LoaderViewHolder(
    binding: ItemLoaderBinding
) : RecyclerView.ViewHolder(binding.root)