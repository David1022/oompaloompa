package com.davidmendano.oompaloompa.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.davidmendano.oompaloompa.databinding.ItemLoaderBinding
import com.davidmendano.oompaloompa.databinding.ItemOompaLoompaBinding
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel

class OLAdapter(
    private val listener: (Int?) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADER = 1

    var list: List<OompaLoompaModel?> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            VIEW_TYPE_ITEM -> {
                OLViewHolder(
                    ItemOompaLoompaBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), listener
                )
            }
            else -> {
                LoaderViewHolder(
                    ItemLoaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is OLViewHolder) {
            holder.bind(list[position])
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int =
        if (list[position] == null) VIEW_TYPE_LOADER else VIEW_TYPE_ITEM
}
