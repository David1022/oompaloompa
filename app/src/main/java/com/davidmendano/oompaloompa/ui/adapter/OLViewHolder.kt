package com.davidmendano.oompaloompa.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.davidmendano.oompaloompa.databinding.ItemOompaLoompaBinding
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel

class OLViewHolder(
    private val binding: ItemOompaLoompaBinding,
    private val listener: (Int?) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: OompaLoompaModel?) {
        binding.root.setOnClickListener {
            listener(model?.id)
        }
        binding.viewModel = model
        setImage(model?.image)
    }

    private fun setImage(image: String?) {
        Glide.with(binding.root)
            .load(image)
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(binding.image)
    }
}