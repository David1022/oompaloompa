package com.davidmendano.oompaloompa.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidmendano.oompaloompa.OLApplication
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.databinding.FragmentDetailBinding
import com.davidmendano.oompaloompa.ui.MainActivity
import javax.inject.Inject

class DetailFragment : Fragment() {

    companion object {
        const val k_OOMPA_LOOMPA = "oompa loompa"
    }

    private lateinit var activity: MainActivity
    private lateinit var binding: FragmentDetailBinding
    private lateinit var viewModel: DetailViewModel

    @Inject
    lateinit var vmFactory: DetailViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)

        initView()
        initObservers()

        return binding.root
    }

    private fun initView() {
        OLApplication.instance.getDagger().inject(this)
        activity = requireActivity() as MainActivity
        viewModel = ViewModelProvider(this, vmFactory).get(DetailViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.loadOompaLoompa(arguments?.getInt(k_OOMPA_LOOMPA))
    }

    private fun initObservers() {
        viewModel.observeViewState().observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                DetailViewModel.DetailState.ERROR -> {
                    activity.showLoader(false)
                    activity.showNetworkError(R.string.generic_error) {
                        viewModel.loadOompaLoompa(arguments?.getInt(k_OOMPA_LOOMPA))
                    }
                }
                DetailViewModel.DetailState.LOADING -> {
                    activity.showLoader(true)
                }
                DetailViewModel.DetailState.SUCCESS -> {
                    activity.showLoader(false)
                }
            }
        })
    }
}