package com.davidmendano.oompaloompa.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.davidmendano.oompaloompa.network.OLRepository
import com.davidmendano.oompaloompa.network.base.ApiWrapper
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel
import kotlinx.coroutines.launch

class DetailViewModel(
    private val repository: OLRepository
) : ViewModel() {

    enum class DetailState { ERROR, LOADING, SUCCESS }

    var oompaLoompa: MutableLiveData<OompaLoompaModel> = MutableLiveData()
    private val viewState: MutableLiveData<DetailState> = MutableLiveData()

    fun observeViewState(): LiveData<DetailState> = viewState

    fun loadOompaLoompa(oompaLoompaId: Int?) {
        if (oompaLoompaId == null) {
            viewState.value = DetailState.ERROR
            return
        }

        viewState.value = DetailState.LOADING
        viewModelScope.launch {
            when (val response = repository.getOLDetail(oompaLoompaId)) {
                is ApiWrapper.Success -> {
                    viewState.value = DetailState.SUCCESS
                    oompaLoompa.value = response.value
                }
                else -> {
                    viewState.value = DetailState.ERROR
                }
            }
        }
    }
}