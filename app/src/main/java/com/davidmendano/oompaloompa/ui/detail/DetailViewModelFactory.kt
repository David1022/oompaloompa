package com.davidmendano.oompaloompa.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.davidmendano.oompaloompa.network.OLRepository
import javax.inject.Inject

class DetailViewModelFactory @Inject constructor(
    private val repository: OLRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        DetailViewModel(repository) as T
}