package com.davidmendano.oompaloompa.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.databinding.DialogFiltersBinding
import com.davidmendano.oompaloompa.utils.Gender
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class FiltersDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogFiltersBinding
    private var gendersList: List<Gender>? = null
    private var professionsList: List<String>? = null
    private lateinit var listener: (Gender?, String?) -> Unit

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogFiltersBinding.inflate(
            LayoutInflater.from(requireContext()),
            container,
            false
        )

        initView()

        return binding.root
    }

    private fun initView() {
        populateGenderList()
        populateProfessionList()
        binding.applyButton.setOnClickListener {
            listener.invoke(getSelectedGender(), getSelectedProfession())
            this.dismiss()
        }
    }

    private fun populateGenderList() {
        if (gendersList.isNullOrEmpty()) {
            binding.gendersTitle.visibility = View.GONE
            binding.gendersRadioGroup.visibility = View.GONE
        } else {
            binding.gendersTitle.visibility = View.VISIBLE
            binding.gendersRadioGroup.visibility = View.VISIBLE
            binding.gendersRadioGroup.addView(
                RadioButton(context).apply {
                    text = getString(R.string.none)
                }
            )
            gendersList?.forEachIndexed { index, gender ->
                binding.gendersRadioGroup.addView(
                    RadioButton(context).apply {
                        id = index
                        text = gender.formattedValue
                    }
                )
            }
        }
    }

    private fun populateProfessionList() {
        if (professionsList.isNullOrEmpty()) {
            binding.professionsTitle.visibility = View.GONE
            binding.professionsRadioGroup.visibility = View.GONE
        } else {
            binding.professionsTitle.visibility = View.VISIBLE
            binding.professionsRadioGroup.visibility = View.VISIBLE
            binding.professionsRadioGroup.addView(
                RadioButton(context).apply {
                    text = getString(R.string.none)
                }
            )
            professionsList?.forEachIndexed { index, profession ->
                binding.professionsRadioGroup.addView(
                    RadioButton(context).apply {
                        id = index
                        text = profession
                    }
                )
            }
        }
    }

    private fun getSelectedGender(): Gender? {
        val checkedID = binding.gendersRadioGroup.checkedRadioButtonId
        if (checkedID < 0 || checkedID >= gendersList?.size ?: 0) {
            return null
        } else {
            return gendersList?.get(checkedID)
        }
    }

    private fun getSelectedProfession(): String? {
        val checkedID = binding.professionsRadioGroup.checkedRadioButtonId
        if (checkedID < 0 || checkedID >= professionsList?.size ?: 0) {
            return null
        } else {
            return professionsList?.get(checkedID)
        }
    }

    companion object {
        fun newInstance(
            genders: List<Gender>?,
            professions: List<String>?,
            listener: (Gender?, String?) -> Unit
        ) =
            FiltersDialog().apply {
                this.gendersList = genders
                this.professionsList = professions
                this.listener = listener
            }
    }
}
