package com.davidmendano.oompaloompa.ui.homelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.davidmendano.oompaloompa.OLApplication
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.databinding.FragmentHomeListBinding
import com.davidmendano.oompaloompa.ui.MainActivity
import com.davidmendano.oompaloompa.ui.adapter.OLAdapter
import com.davidmendano.oompaloompa.ui.detail.DetailFragment
import com.davidmendano.oompaloompa.ui.dialogs.FiltersDialog
import com.davidmendano.oompaloompa.utils.Gender
import javax.inject.Inject

class HomeListFragment : Fragment() {

    private lateinit var binding: FragmentHomeListBinding
    private lateinit var activity: MainActivity
    private lateinit var adapter: OLAdapter

    @Inject
    lateinit var vmFactory: HomeListViewModelFactory
    private lateinit var viewModel: HomeListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeListBinding.inflate(inflater, container, false)
        initView()
        initObservers()
        viewModel.loadMoreData()

        return binding.root
    }

    private fun initView() {
        OLApplication.instance.getDagger().inject(this)
        activity = requireActivity() as MainActivity
        viewModel = ViewModelProvider(this, vmFactory).get(HomeListViewModel::class.java)
        binding.filterFab.setOnClickListener { viewModel.filterClicked() }
        initRecycler()
    }

    private fun initRecycler() {
        this.adapter = OLAdapter { viewModel.onItemClicked(it) }
        binding.recycler.adapter = this.adapter
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        binding.recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                viewModel.onListScrolled(
                    (binding.recycler.layoutManager as LinearLayoutManager)
                        .findLastVisibleItemPosition()
                )
            }
        })

    }

    private fun initObservers() {
        viewModel.observeViewState().observe(viewLifecycleOwner,
            Observer {
                handleLoadState(it)
            })
        viewModel.observeList().observe(viewLifecycleOwner,
            Observer { list ->
                if (list != null) {
                    this.adapter.list = list
                }
            })
    }

    private fun handleLoadState(viewState: OLViewState) {
        when (viewState.state) {
            OLViewState.OLState.NONE -> {
                // Do nothing
            }
            OLViewState.OLState.ERROR -> {
                activity.showNetworkError(viewState.errorMessage) {
                    viewModel.loadMoreData()
                }
            }
            OLViewState.OLState.GO_TO_DETAIL -> {
                viewModel.resetView()
                findNavController().navigate(
                    R.id.action_homeListFragment_to_detailFragment,
                    bundleOf(DetailFragment.k_OOMPA_LOOMPA to viewState.oompaLoompaId)
                )
            }
            OLViewState.OLState.FILTER -> {
                showFilters(
                    viewState.gendersList,
                    viewState.professionsList
                )
            }
        }
    }

    private fun showFilters(
        gendersList: List<Gender>?,
        professionsList: List<String>?
    ) {
        FiltersDialog.newInstance(
            gendersList,
            professionsList
        ) { gender, profession ->
            viewModel.filtersSelected(gender, profession)
        }.show(parentFragmentManager, "Filter dialog")
    }
}