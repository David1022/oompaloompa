package com.davidmendano.oompaloompa.ui.homelist

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.network.OLRepository
import com.davidmendano.oompaloompa.network.base.ApiWrapper
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel
import com.davidmendano.oompaloompa.utils.Gender
import kotlinx.coroutines.launch

class HomeListViewModel(private val repository: OLRepository) : ViewModel() {

    private val DEFAULT_CURRENT_PAGE = 0
    private val DEFAULT_TOTAL_PAGES = 1

    private var isLoading: Boolean = false
    private var genderFilter: Gender? = null
    private var professionFilter: String? = null

    private var gendersList: List<Gender> = mutableListOf(Gender.MALE, Gender.FEMALE)
    private var professionsList: List<String> = mutableListOf()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal var currentPage: Int = DEFAULT_CURRENT_PAGE

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal var totalPages: Int = DEFAULT_TOTAL_PAGES

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal val viewState: MutableLiveData<OLViewState> = MutableLiveData()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal val oompaLoompaList: MutableLiveData<MutableList<OompaLoompaModel?>?> =
        MutableLiveData()

    private val filteredList: MutableLiveData<MutableList<OompaLoompaModel?>?> = MutableLiveData()

    fun observeViewState(): LiveData<OLViewState> = viewState

    fun observeList(): LiveData<MutableList<OompaLoompaModel?>?> = filteredList

    fun loadMoreData(lastVisiblePosition: Int? = null) {
        if ((oompaLoompaList.value?.size ?: 0 == 0) || canLoadMoreItems(lastVisiblePosition)) {
            addLoaderItem()
            loadData()
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal fun loadData() {
        viewModelScope.launch {
            when (val response = repository.getOLList(currentPage + 1)) {
                is ApiWrapper.Success -> {
                    removeLoaderItem()
                    response.value.results?.let {
                        oompaLoompaList.value?.addAll(it)
                    }
                    currentPage = response.value.current ?: DEFAULT_CURRENT_PAGE
                    totalPages = response.value.total ?: DEFAULT_TOTAL_PAGES
                    updateFilterLists(oompaLoompaList.value)
                    applyFilter()
                    notifyListChanged()
                }
                else -> {
                    removeLoaderItem()
                    viewState.value = OLViewState.showError(R.string.generic_error)
                }
            }
        }
    }

    private fun updateFilterLists(list: MutableList<OompaLoompaModel?>?) {
        if (list.isNullOrEmpty()) {
            professionsList = mutableListOf()
            return
        }

        professionsList = list.distinctBy { it?.profession }.mapNotNull { it?.profession }
    }

    private fun notifyListChanged() {
        oompaLoompaList.value = oompaLoompaList.value
    }

    fun onItemClicked(id: Int?) {
        viewState.value = OLViewState.goToDetail(id)
    }

    fun onListScrolled(lastVisiblePosition: Int? = null) {
        loadMoreData(lastVisiblePosition)
    }

    private fun addLoaderItem() {
        isLoading = true
        val currentList = oompaLoompaList.value
        if (currentList != null) {
            currentList.add(null)
            oompaLoompaList.value = currentList
            applyFilter()
        } else {
            oompaLoompaList.value = mutableListOf(null)
            applyFilter()
        }
    }

    private fun removeLoaderItem() {
        isLoading = false
        oompaLoompaList.value?.let { list ->
            if (list[list.size - 1] == null) {
                list.removeAt(list.size - 1)
            }
        }
        applyFilter()
    }

    private fun canLoadMoreItems(lastVisiblePosition: Int?): Boolean =
        currentPage < totalPages &&
                !isLoading && isLastItemVisible(lastVisiblePosition)

    /**
     * When last item is visible, should request more items in order to have continuous loading
     * until the last item is requested and shown
     */
    private fun isLastItemVisible(lastVisiblePosition: Int?): Boolean =
        lastVisiblePosition != null &&
                lastVisiblePosition >= (oompaLoompaList.value?.size ?: 0) - 1

    fun resetView() {
        viewState.value = OLViewState.resetView()
    }

    fun filterClicked() {
        viewState.value =
            OLViewState.showFilters(gendersList, professionsList)
    }

    fun filtersSelected(gender: Gender?, profession: String?) {
        genderFilter = gender
        professionFilter = profession
        applyFilter()
    }

    private fun applyFilter() {
        var fList: MutableList<OompaLoompaModel?>? = oompaLoompaList.value
        genderFilter?.let { gndr ->
            fList = fList?.filter { it?.gender == gndr.apiValue }?.toMutableList()
        }
        professionFilter?.let { prof ->
            fList = fList?.filter { it?.profession == prof }?.toMutableList()
        }
        filteredList.value = fList
    }
}