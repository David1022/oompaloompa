package com.davidmendano.oompaloompa.ui.homelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.davidmendano.oompaloompa.network.OLRepository
import javax.inject.Inject

class HomeListViewModelFactory @Inject constructor(private val repository: OLRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        HomeListViewModel(repository) as T
}