package com.davidmendano.oompaloompa.ui.homelist

import com.davidmendano.oompaloompa.utils.Gender

class OLViewState(
    val state: OLState,
    val errorMessage: Int? = null,
    val oompaLoompaId: Int? = null,
    val gendersList: List<Gender>? = null,
    val professionsList: List<String>? = null,
) {
    enum class OLState {
        NONE,
        ERROR,
        GO_TO_DETAIL,
        FILTER
    }

    companion object {
        fun resetView(): OLViewState =
            OLViewState(OLState.NONE)

        fun showError(errorMessage: Int?): OLViewState =
            OLViewState(OLState.ERROR, errorMessage = errorMessage)

        fun goToDetail(id: Int?): OLViewState =
            OLViewState(OLState.GO_TO_DETAIL, oompaLoompaId = id)

        fun showFilters(
            gendersList: List<Gender>,
            professionsList: List<String>
        ): OLViewState =
            OLViewState(
                OLState.FILTER,
                gendersList = gendersList,
                professionsList = professionsList
            )
    }
}