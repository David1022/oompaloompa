package com.davidmendano.oompaloompa.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.davidmendano.oompaloompa.R
import com.davidmendano.oompaloompa.databinding.FragmentSplashBinding

class SplashFragment : Fragment() {

        private lateinit var binding: FragmentSplashBinding

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding = FragmentSplashBinding.inflate(inflater, container, false)

            binding.splashTitle.animation = getFadeInAnim()

            return binding.root
        }

        private fun getFadeInAnim(): AlphaAnimation =
            AlphaAnimation(0f, 1f).apply {
                interpolator = AccelerateInterpolator() //add this
                duration = 1000
                setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(animation: Animation?) {
                        // Do nothing
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        findNavController().navigate(R.id.action_splashFragment_to_homeListFragment)
                    }

                    override fun onAnimationStart(animation: Animation?) {
                        // Do nothing
                    }

                })
            }
    }
