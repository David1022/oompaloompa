package com.davidmendano.oompaloompa.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

@BindingAdapter("ol_image")
fun setOlImage(view: AppCompatImageView, image: String?) {
    Glide.with(view.context)
        .load(image)
        .centerCrop()
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .into(view)
}