package com.davidmendano.oompaloompa.utils

enum class Gender(val apiValue: String, val formattedValue: String) {
    MALE("M", "Male"),
    FEMALE("F", "Female")
}