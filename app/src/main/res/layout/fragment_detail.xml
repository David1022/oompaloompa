<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.davidmendano.oompaloompa.ui.detail.DetailViewModel" />
    </data>

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            tools:context=".ui.detail.DetailFragment">

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/image"
                android:layout_width="@dimen/image_size"
                android:layout_height="@dimen/image_size"
                android:layout_marginVertical="@dimen/margin_medium"
                android:layout_marginStart="@dimen/margin_medium"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:ol_image="@{viewModel.oompaLoompa.image}"
                tools:srcCompat="@tools:sample/avatars" />

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/name"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginHorizontal="@dimen/margin_medium"
                android:ellipsize="end"
                android:maxLines="1"
                android:text="@{viewModel.oompaLoompa.formattedName}"
                android:textSize="@dimen/list_text_name"
                android:textStyle="bold"
                app:layout_constraintBottom_toTopOf="@id/profession"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toEndOf="@id/image"
                app:layout_constraintTop_toTopOf="@id/image"
                tools:text="@tools:sample/lorem/random" />

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/profession"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginHorizontal="@dimen/margin_medium"
                android:ellipsize="end"
                android:maxLines="1"
                android:text="@{viewModel.oompaLoompa.profession}"
                android:textSize="@dimen/list_text_profession"
                app:layout_constraintBottom_toTopOf="@id/gender"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toEndOf="@id/image"
                app:layout_constraintTop_toBottomOf="@id/name"
                tools:text="@tools:sample/lorem/random" />

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/gender"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginHorizontal="@dimen/margin_medium"
                android:ellipsize="end"
                android:maxLines="1"
                android:text="@{viewModel.oompaLoompa.formattedGender}"
                android:textAllCaps="true"
                android:textSize="@dimen/list_text_gender"
                app:layout_constraintBottom_toBottomOf="@id/image"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toEndOf="@id/image"
                app:layout_constraintTop_toBottomOf="@id/profession"
                tools:text="@tools:sample/lorem/random" />

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/info_container"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/margin_medium"
                android:background="@drawable/background_corner_radius"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/image">

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/info_title"
                    style="@style/DetailLargeTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:layout_marginBottom="@dimen/margin_medium"
                    android:text="@string/info"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />


                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/country_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:text="@string/country"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/info_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/country_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.country}"
                    app:layout_constraintBaseline_toBaselineOf="@id/country_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/country_title"
                    tools:text="@tools:sample/lorem/random" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/email_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:text="@string/email"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/country_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/email_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.email}"
                    app:layout_constraintBaseline_toBaselineOf="@id/email_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/email_title"
                    tools:text="@tools:sample/lorem/random" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/age_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:text="@string/age"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/email_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/age_title_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.formattedAge}"
                    app:layout_constraintBaseline_toBaselineOf="@id/age_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/age_title"
                    tools:text="@tools:sample/lorem/random" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/height_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginVertical="@dimen/margin_medium"
                    android:text="@string/height"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/age_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/height_title_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.formattedHeight}"
                    app:layout_constraintBaseline_toBaselineOf="@id/height_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/height_title"
                    tools:text="@tools:sample/lorem/random" />
            </androidx.constraintlayout.widget.ConstraintLayout>

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/favorite_container"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/margin_medium"
                android:background="@drawable/background_corner_radius"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/info_container">

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/favorite_title"
                    style="@style/DetailLargeTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:layout_marginBottom="@dimen/margin_medium"
                    android:text="@string/favorite"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/food_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:text="@string/food"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/favorite_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/food_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.favorite.food}"
                    app:layout_constraintBaseline_toBaselineOf="@id/food_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/food_title"
                    tools:text="@tools:sample/lorem/random" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/song_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:text="@string/song"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/food_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/song_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginTop="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.favorite.song}"
                    app:layout_constraintBaseline_toBaselineOf="@id/song_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/song_title"
                    tools:text="@tools:sample/lorem/random" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/color_title"
                    style="@style/DetailTitleStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:layout_marginVertical="@dimen/margin_medium"
                    android:text="@string/color"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/song_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/color_title_value"
                    style="@style/DetailTextStyle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/margin_medium"
                    android:ellipsize="end"
                    android:maxLines="1"
                    android:text="@{viewModel.oompaLoompa.favorite.color}"
                    app:layout_constraintBaseline_toBaselineOf="@id/color_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/color_title"
                    tools:text="@tools:sample/lorem/random" />

            </androidx.constraintlayout.widget.ConstraintLayout>

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/description_title"
                style="@style/DetailLargeTitleStyle"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginHorizontal="@dimen/margin_medium"
                android:layout_marginTop="@dimen/margin_medium"
                android:layout_marginBottom="@dimen/margin_medium"
                android:text="@string/description"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/favorite_container" />

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/description_value"
                style="@style/DetailTextStyle"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginHorizontal="@dimen/margin_medium"
                android:layout_marginTop="@dimen/margin_medium"
                android:ellipsize="end"
                android:text="@{viewModel.oompaLoompa.favorite.randomString}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/description_title"
                tools:text="@tools:sample/lorem/random" />

        </androidx.constraintlayout.widget.ConstraintLayout>
    </ScrollView>
</layout>