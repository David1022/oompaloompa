package com.davidmendano.oompaloompa.ui.homelist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.davidmendano.oompaloompa.network.OLRepository
import com.davidmendano.oompaloompa.network.base.ApiWrapper
import com.davidmendano.oompaloompa.network.model.OLListModel
import com.davidmendano.oompaloompa.network.model.OompaLoompaModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class HomeListViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    var instantTarget: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var viewStateObserver: Observer<OLViewState>

    @Mock
    lateinit var listObserver: Observer<MutableList<OompaLoompaModel?>?>

    @Mock
    lateinit var repository: OLRepository

    lateinit var viewModel: HomeListViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        MockitoAnnotations.initMocks(this)
        viewModel = HomeListViewModel(repository)
        viewModel.observeViewState().observeForever(viewStateObserver)
        viewModel.observeList().observeForever(listObserver)
        viewModel.viewState.value = null
        viewModel.oompaLoompaList.value = null
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `when user click an item should show detail`() {
        val oompaLoompa = OompaLoompaModel(id = 1234)
        viewModel.onItemClicked(oompaLoompa.id)

        assert(viewModel.viewState.value?.state == OLViewState.OLState.GO_TO_DETAIL)
        assert(viewModel.viewState.value?.oompaLoompaId == oompaLoompa.id)
    }

    // ********************
    // GETTING MORE ITEMS
    // ********************

    @Test
    fun `when getting items returns Network Error should show error`() {
        runBlockingTest {
            Mockito
                .`when`<Any>(repository.getOLList(ArgumentMatchers.anyInt()))
                .thenReturn(ApiWrapper.NetworkError)
            viewModel.loadMoreData()
        }

        assert(viewModel.viewState.value?.state == OLViewState.OLState.ERROR)
    }

    @Test
    fun `when getting items returns Server Error should show error`() {
        runBlockingTest {
            Mockito
                .`when`<Any>(repository.getOLList(ArgumentMatchers.anyInt()))
                .thenReturn(ApiWrapper.ServerError)
            viewModel.loadMoreData()
        }

        assert(viewModel.viewState.value?.state == OLViewState.OLState.ERROR)
    }

    @Test
    fun `when getting items returns Success should add new items to list`() {
        val response = OLListModel(
            current = 0,
            total = 1,
            results = mutableListOf(OompaLoompaModel(), OompaLoompaModel())
        )
        runBlockingTest {
            Mockito
                .`when`<Any>(repository.getOLList(ArgumentMatchers.anyInt()))
                .thenReturn(ApiWrapper.Success(response))
            viewModel.loadMoreData()
        }

        assert(viewModel.oompaLoompaList.value?.size == response.results?.size)
        assert(viewModel.oompaLoompaList.value?.last() != null)
    }
}